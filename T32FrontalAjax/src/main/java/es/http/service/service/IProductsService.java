package es.http.service.service;

import java.util.List;

import es.http.service.dto.Product;

public interface IProductsService {

	public List<Product> listarProducts();
	
	public Product guardarProducts(Product product);
	
	public Product encontrarProductId(int id);
	
	public Product actualizarProduct(Product product);
	
	public void eliminarProduct(int id);
}
