package es.http.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.http.service.dao.IProductsDao;
import es.http.service.dto.Product;

@Service
public class ProductsServiceImpl implements IProductsService {
	
	@Autowired
	IProductsDao iProductsDao;
	
	@Override
	public List<Product> listarProducts() {
		return iProductsDao.findAll();
	}
	
	@Override
	public Product guardarProducts(Product product) {
		return iProductsDao.save(product);
	}
	
	@Override
	public Product encontrarProductId(int id) {
		return iProductsDao.findById(id).get();
	}
	
	@Override
	public Product actualizarProduct(Product product) {
		return iProductsDao.save(product);
	}
	
	@Override
	public void eliminarProduct(int id) {
		iProductsDao.deleteById(id);
	}
}
