package es.http.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T32FrontalAjaxApplication {

	public static void main(String[] args) {
		SpringApplication.run(T32FrontalAjaxApplication.class, args);
	}

}
