package es.http.service.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import es.http.service.dto.Product;

public interface IProductsDao extends JpaRepository<Product, Integer> {

}
