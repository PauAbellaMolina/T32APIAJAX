package es.http.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import es.http.service.dto.Product;
import es.http.service.service.ProductsServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE})
@RequestMapping("/api")
public class ProductsController {

	@Autowired
	ProductsServiceImpl productsServiceImpl;
	
	@GetMapping("/products")
	public List<Product> listarProductsController() {
		return productsServiceImpl.listarProducts();
	}
	
	@PostMapping("/products")
	public Product guardarProductsController(@RequestBody Product product) {
		return productsServiceImpl.guardarProducts(product);
	}
	
	@GetMapping("/products/{id}")
	public Product encontrarEquipoIdController(@PathVariable(name="id") int id){
		Product productEncontrado = new Product();
		productEncontrado = productsServiceImpl.encontrarProductId(id);
		return productEncontrado;
	}
	
	@PutMapping("/products/{id}")
	public Product actualizarProductsController(@PathVariable(name="id") int id, @RequestBody Product product){
		Product productSeleccionado = new Product();
		Product productActualizado = new Product();
		
		productSeleccionado = productsServiceImpl.encontrarProductId(id);
		
		productSeleccionado.setName(product.getName());
		
		productActualizado = productsServiceImpl.actualizarProduct(productSeleccionado);
		
		return productActualizado;
	}
	
	@DeleteMapping("/products/{id}")
	public void eliminarProductController(@PathVariable(name="id") int id) {
		productsServiceImpl.eliminarProduct(id);
	}
}
